﻿namespace BowlingGame
{
    public class Frame
    {
        public bool FirstTurn { get; private set; }
        public bool LastShoot { get; set; }

        public int TotalPins { get; private set; }
        public bool StrikeBonus { get; private set; }
        public bool SpareBonus { get; private set; }
        public bool Completed { get; private set; }

        public Frame()
        {
            LastShoot = false;
            FirstTurn = true;
            TotalPins = 0;
            StrikeBonus = false;
            SpareBonus = false;
            Completed = false;
        }

        public void Roll(int pins)
        {
            TotalPins += pins;

            if (FirstTurn)
            {
                FirstTurn = false;

                if (TotalPins == 10)
                {
                    StrikeBonus = true;
                    Completed = true;
                }
            }
            else
            {
                if (TotalPins == 10)
                {
                    SpareBonus = true;
                }

                Completed = true;
            }
        }
    }
}