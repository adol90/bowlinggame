﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace BowlingGame
{
    [TestFixture]
    public class FrameTest
    {
        private Frame _frame;
        private readonly int _topScore = 10;
        private readonly int _score = 5;

        [SetUp]
        public void SetUp()
        {
            _frame = new Frame();
        }

        [TearDown]
        public void TearDown()
        {
            _frame = null;
        }

        [Test]
        public void should_activate_spare_bonus_if_rolls_ten_pins_in_two_turns()
        {
            
            _frame.Roll(_score);
            _frame.Roll(_score);

            Assert.IsTrue(_frame.SpareBonus);
        }

        [Test]
        public void should_activate_strike_bonus_if_rolls_ten_pins_in_one_turn()
        {
            _frame.Roll(_topScore);

            Assert.IsTrue(_frame.StrikeBonus);
        }

        [Test]
        public void should_complete_then_frame_if_throw_ten_pins_in_one_turn()
        {
            _frame.Roll(_topScore);

            Assert.IsTrue(_frame.Completed);
        }

        [TestCase(5,5)]
        [TestCase(0, 0)]
        public void should_complete_the_frame_if_there_was_a_roll_at_second_turn(int firstShoot, int secondShoot)
        {
            _frame.Roll(firstShoot);
            _frame.Roll(secondShoot);

            Assert.IsTrue(_frame.Completed);
        }

        [TestCase(5)]
        [TestCase(10)]
        public void should_be_able_to_roll_three_times_if_lastFrame(int score)
        {
            _frame.Roll(this._score);
            _frame.Roll(this._score);
            _frame.Roll(this._score);

            Assert.AreEqual(_frame.TotalPins, this._score*3);
        }


    }
}
