﻿using System;
using System.Collections.Generic;

namespace BowlingGame
{
    internal class Game
    {
        private Frame currentFrame;
        private IList<Frame> frames;
        private int totalScore;
        private int currentFrameIndex;
        private readonly int MAX_FRAMES = 10;
        private readonly int MAX_SCORE = 10;

        public Game(IList<Frame> frames)
        {
            this.frames = frames;
            totalScore = 0;
            currentFrameIndex = 0;
        }

        public void Roll(int score)
        {
            currentFrame = frames[currentFrameIndex];
            CheckScoreValue(score);
            NextTurnOrFinish();
            RollPins(score);
        }

        private void CheckScoreValue(int score)
        {
            if (score < 0 || score > MAX_SCORE)
                throw new ArgumentOutOfRangeException();
        }

        private void NextTurnOrFinish()
        {
            if (!currentFrame.Completed)
                return;

            if (currentFrameIndex >= MAX_FRAMES)
                CheckIfGameHasFinished();

            currentFrameIndex++;

            var newFrame = new Frame();
            if (currentFrameIndex >= MAX_FRAMES + 2)
                newFrame.LastShoot = true;

            frames.Add(newFrame);

        }

        private void CheckIfGameHasFinished()
        {
            if(currentFrame.LastShoot)
                throw new ApplicationException("You have already finished the game");

            if (!currentFrame.SpareBonus && !currentFrame.StrikeBonus)
                throw new ApplicationException("You have already finished the game");

            if (currentFrame.StrikeBonus && currentFrameIndex >= MAX_FRAMES + 1)
                throw new ApplicationException("You have already finished the game");

            if (currentFrame.SpareBonus && currentFrameIndex >= MAX_FRAMES)
                throw new ApplicationException("You have already finished the game");
        }

        private void RollPins(int score)
        {
            AssignCurrentFrameIndex();
            CalculateScoreAndBonus(score);
            currentFrame.Roll(score);
        }

        private void AssignCurrentFrameIndex()
        {
            currentFrame = frames[currentFrameIndex];
        }

        private void CalculateScoreAndBonus(int score)
        {
            if (NotFirstTurn())
            {
                Frame previousFrame = frames[currentFrameIndex - 1];

                if (NotFirstNorSecondTurn())
                {
                    Frame twoPreviousFrame = frames[currentFrameIndex - 2];
                    if (twoPreviousFrame.StrikeBonus && previousFrame.StrikeBonus)
                    {
                        totalScore += score * 2 + previousFrame.TotalPins;
                        return;
                    }
                }

                if (previousFrame.StrikeBonus)
                {
                    totalScore += score * 2;
                    return;
                }
                if (previousFrame.SpareBonus && currentFrame.FirstTurn)
                {
                    totalScore += score * 2;
                    return;
                } 
            }

            totalScore += score;
        }

        private bool NotFirstNorSecondTurn()
        {
            return currentFrameIndex > 1;
        }

        private bool NotFirstTurn()
        {
            return currentFrameIndex > 0;
        }

        public int Score()
        {
            return totalScore;
        }
    }
}