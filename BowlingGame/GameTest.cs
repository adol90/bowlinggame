﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace BowlingGame
{
    public class GameTest
    {
        private const int MAX_FRAMES = 10;
        private Game _game;
        private Mock<IList<Frame>> _listOfFrames;
        private const int TopScore = 10;
        private const int MidScore = 5;
        private const int LowScore = 3;

        [SetUp]
        public void SetUp()
        {
            _listOfFrames = new Mock<IList<Frame>>();
            _listOfFrames.Setup(m => m.Add(It.IsAny<Frame>()));
            SetUpFramesMock();
            _game = new Game(_listOfFrames.Object);
        }

        private void SetUpFramesMock()
        {
            for (int i=0; i <= MAX_FRAMES+2 ; i++)
                _listOfFrames.SetupGet(m => m[i]).Returns(new Frame());
        }

        [TearDown]
        public void TearDown()
        {
            _game = null;
        }

        [TestCase(-1)]
        [TestCase(11)]
        [Test]
        public void should_crash_if_the_score_passed_is_more_than_ten(int scoreToTest)
        {

            Assert.Throws<ArgumentOutOfRangeException>(() => { _game.Roll(scoreToTest); });
        }

        [Test]
        public void should_crash_if_the_game_has_finished_and_try_to_shoot_again()
        {
            for (int i = 0; i <= MAX_FRAMES ; i++)
            {
                _game.Roll(LowScore);
                _game.Roll(LowScore);
            }

            Assert.Throws<ApplicationException>(() => { _game.Roll(LowScore); }, "You have already finished the game");
        }

        [Test]
        public void should_crash_after_perform_strike_in_three_last_turns_if_continues_shooting()
        {
            for (int i = 0; i < MAX_FRAMES+2 ; i++)
            {
                _game.Roll(TopScore);
            }

            Assert.Throws<ApplicationException>(() => { _game.Roll(LowScore); }, "You have already finished the game");
        }

        [Test]
        public void should_crash_after_perform_spare_in_two_last_turns_if_continues_shooting()
        {
            for (int i = 0; i < MAX_FRAMES+1; i++)
            {
                _game.Roll(MidScore);
                _game.Roll(MidScore);
            }

            Assert.Throws<ApplicationException>(() => { _game.Roll(MidScore); }, "You have already finished the game");
        }

        [Test]
        public void should_crash_after_perform_strike_in_two_last_turns_and_fail_last_shoot_if_continues_shooting()
        {
            int fail = 0;

            for (int i = 0; i < MAX_FRAMES + 1; i++)
            {
                _game.Roll(TopScore);
            }

            _game.Roll(fail);
            _game.Roll(fail);

            Assert.Throws<ApplicationException>(() => { _game.Roll(fail); }, "You have already finished the game");
        }


        [TestCase(5)]
        [TestCase(10)]
        public void should_not_crash_if_the_game_has_finished_but_there_is_bonus_in_last_frame(int lastScore)
        {
            for (int i = 0; i < MAX_FRAMES - 2; i++)
            {
                _game.Roll(LowScore);
                _game.Roll(LowScore);
            }

            _game.Roll(lastScore);
            _game.Roll(lastScore);
            _game.Roll(lastScore);

            Assert.DoesNotThrow(() => { _game.Roll(LowScore); }, "You have already finished the game");
        }


        [TestCase(8,5,3)]
        [TestCase(6, 4,1)]
        [TestCase(5, 5,2)]
        [TestCase(0, 0,8)]
        public void should_start_a_new_frame_if_previous_one_has_ended(int firstScore, int secondScore, int thirdScore)
        {
            _game.Roll(firstScore);
            _game.Roll(secondScore);
            _game.Roll(thirdScore);

            _listOfFrames.Verify(m => m.Add(It.IsAny<Frame>()), Times.Once);
        }

        [Test]
        public void should_calculate_score_for_a_game_without_bonuses()
        {
            for (int i = 0; i < MAX_FRAMES; i++)
            {
                _game.Roll(LowScore);
                _game.Roll(LowScore);
            }

            Assert.AreEqual(LowScore* 2 * (MAX_FRAMES), _game.Score());
        }


        [Test]
        public void should_calculate_score_for_bonus_game()
        {
            _game.Roll(TopScore);

            for (int i = 0; i < MAX_FRAMES - 1; i++)
            {
                _game.Roll(LowScore);
                _game.Roll(LowScore);
            }

            Assert.AreEqual((LowScore * 2 * MAX_FRAMES) + TopScore, _game.Score());

        }

        [Test]
        public void should_calculate_properly_for_bonus_a_max_score_game()
        {
            for (int i = 0; i < MAX_FRAMES + 2; i++)
            {
                _game.Roll(TopScore);
            }

            Assert.AreEqual(TopScore* 3 * MAX_FRAMES + TopScore * 3, _game.Score());

        }

        [Test]
        public void should_calculate_properly_for_bonus_a_full_spare_bonus_game()
        {
            for (int i = 0; i < MAX_FRAMES + 1; i++)
            {
                _game.Roll(MidScore);
                _game.Roll(MidScore);
            }

            Assert.AreEqual(MidScore * 2 * MAX_FRAMES + MidScore*10 + MidScore*2 , _game.Score());
        }

        [Test]
        public void should_calculate_score_for_random_game()
        {
            _game.Roll(LowScore);
            _game.Roll(LowScore);
            _game.Roll(TopScore);
            _game.Roll(MidScore);
            _game.Roll(MidScore);
            _game.Roll(LowScore);
            _game.Roll(LowScore);
            _game.Roll(LowScore);
            _game.Roll(LowScore);
            _game.Roll(MidScore);
            _game.Roll(MidScore);
            _game.Roll(TopScore);
            _game.Roll(LowScore);
            _game.Roll(LowScore);
            _game.Roll(MidScore);
            _game.Roll(MidScore);
            _game.Roll(LowScore);
            _game.Roll(LowScore);

            Assert.AreEqual(112, _game.Score());
        }





    }
}
